
const request = require('supertest')
const app = require('../app')
const appMock = require('../mock/server-mock')

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(200)
      .expect(/Express/, done)
  })

  it('has the default mocked server', function(done) {
    request(appMock)
      .get('/api/v1/mock')
      .expect(200)
      .expect(/Mocked Server/, done)
  })

  it('has the default mocked server', function(done) {
    request(appMock)
      .get('/api/v1/mock')
      .expect(200)
      .expect(/Mocked Server/, done)
  })
}) 
