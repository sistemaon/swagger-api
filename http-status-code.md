
# HyperText Transfer Protocol (HTTP)

# Successful responses


### 200 ok

**Request** has _succeeded_.

---

### 201 Created

**Request** has _succeeded_ and a new resource has been _created_ as a result.

---

### 202 Accepted

**Request** has been _received_ but not yet acted upon.

---

### 204 No Content

**Request** indicates that has succeeded, but that the client doesn't need to go away from its current page. <br>

> The successful result of a PUT or a DELETE is often not a 200 OK but a 204 No Content (or a 201 Created when the resource is uploaded for the first time).

---

<br>

---

# Bad responses


### 400 Bad Request 

**Request** indicates that the server _cannot_ or will _not_ process the request due to something that is _perceived_ to be a client _error_.

---

### 500 Internal Server Error

**Request**  indicates that the server encountered an _unexpected_ condition that _prevented_ it from fulfilling the request.

---



---

> References:
> - https://developer.mozilla.org/en-US/docs/Web/HTTP/Status