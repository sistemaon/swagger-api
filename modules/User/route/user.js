
// USER ROUTE

const express = require('express')
const router = express.Router()

const User = require('../controller/user')

// router endpoint to create user
router.post('/user', User.create)

module.exports = router
