
// USER MODEL

const Sequelize = require('sequelize')

const sequelize = require('../../../configs/db.config')

// defines sequelize instance table
const User = sequelize.define('user', {
  // creating model attributes
  user_id:   { primaryKey: true, type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4 },
  user_name: { type: Sequelize.STRING },
  user_identifier: { type: Sequelize.STRING },
  city: { type: Sequelize.STRING },
  country: { type: Sequelize.STRING },
  email: { type: Sequelize.STRING },
  phone: { type: Sequelize.STRING },
  birthday: { type: Sequelize.STRING },
  user_type: { type: Sequelize.ENUM({ values: ['User', 'Influencer'] }) },
  is_pro: { type: Sequelize.BOOLEAN },
  payed_options: { type: Sequelize.ENUM({ values: ['Basic', 'Normal', 'Pro'] }) },
  height: { type: Sequelize.FLOAT },
  weight: { type: Sequelize.FLOAT },
  sport_interest: { type: Sequelize.ARRAY(Sequelize.STRING) }
})

module.exports = User
