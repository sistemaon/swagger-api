
// USER CONTROLLER

const User = require('../model/user')

// function to create user
const create = async (req, res, next) => {

	try {

		const userObj = {
			user_name: 			req.body.userName 			|| 'Janie Rond',
			user_identifier: 	req.body.userIdentifier 	|| '@double_08',
			city: 				req.body.city 				|| 'Irvine',
			country: 			req.body.country 			|| 'USA',
			email: 				req.body.email 				|| '008@agent.cc',
			phone: 				req.body.phone 				|| '(+1) - 333666999',
			birthday: 			req.body.birthday 			|| '1992-02-25',
			user_type: 			req.body.userType 			|| 'Influencer',
			is_pro: 			req.body.isPayed 			|| false,
			payed_options: 		req.body.payedOptions 		|| 'Normal',
			height: 			req.body.height 			|| 1.71,
			weight: 			req.body.weight 			|| 71.1,
			sport_interest:		req.body.sportInterest 		|| [ "shooting", "running", "skydiving" ]
		}
		// console.info('userObj ::; ', userObj)

		// creates user info in database
		const user = await User.create(userObj)
		// console.info('user ::; ', user)

		const userRes = {
			message: 			"created",
			userId: 			user.user_id,
			userName: 			user.user_name,
			userIdentifier: 	user.user_identifier
		}

		res.status(201).json(userRes)

	} catch (error) {

		res.status(400).json(error)

	}
}

const userControllers = {
	create
}

module.exports = userControllers
