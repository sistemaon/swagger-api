
// Sequelize constructor options reference, search for Public Constructors for more information
// https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor


const Sequelize = require('sequelize')

// configuration environment from configs file environment.config.example.js
const environmentConfig = require('./environment.config') // environment.config.example

// new Sequelize instance
//                     // 'database name', 'user', 'password'
const sequelize = new Sequelize(environmentConfig.DB_NAME, environmentConfig.DB_USER, environmentConfig.DB_PASSWORD, {
  host: environmentConfig.DB_HOST,
  port: environmentConfig.DB_PORT,
  dialect: environmentConfig.DB_DIALECT,

  // search for pool options for more information
  // http://docs.sequelizejs.com/class/lib/sequelize.js~Sequelize.html
  pool: {
    // Maximum number of connection in pool
    max: 9,

    // Minimum number of connection in pool
    min: 0,

    // The maximum time, in milliseconds, that a connection can be idle before being released.
    idle: 10000,

    // The maximum time, in milliseconds, that pool will try to get connection before throwing error
    acquire: 30000,

    // The time interval, in milliseconds, after which sequelize-pool will remove idle connections.
    evict: 10000
  }

})

// checks sequelize if connection has been made, or it's unavailable
sequelize
  .authenticate()
  .then(() => {
    console.info('Connection has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database ::; ', err)
  })

// creates tables automatically if does not exists
sequelize.sync()

module.exports = sequelize