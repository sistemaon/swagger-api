
// server.js
const jsonServer = require('json-server')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const dbPath = './mock/db.json'
const server = jsonServer.create()
const router = jsonServer.router(dbPath)
const middlewares = jsonServer.defaults()

const adapter = new FileSync(dbPath)
const db = low(adapter)

server.use(middlewares)
server.use(jsonServer.bodyParser)

// USERS
server.post('/api/v1/user', (req, res) => {

  const userReqBody = req.body
  
  const userObj = {
    user_id:          userReqBody.userId,           // (String) (Not Null)
    user_name:        userReqBody.userName,         // (String) (Not Null)
    user_identifier:  userReqBody.userIdentifier,   // (String) (Not Null)
    city:             userReqBody.city,             // (String) (Not Null)
    country:          userReqBody.country,          // (String) (Not Null)
    email:            userReqBody.email,            // (String) (Not Null)
    phone:            userReqBody.phone,            // (String) (Nullable)
    birthday:         userReqBody.birthday,         // (String) (Nullable)
    user_type:        userReqBody.userType,         // (<Enumerate<String>>) (Not Null)
    is_pro:           userReqBody.isPayed,          // (Boolean) (Not Null)
    payed_options:    userReqBody.payedOptions,     // (<Enumerate<String>>) (Nullable)
    height:           userReqBody.height,           // (Float) (Not Null)
    weight:           userReqBody.weight,           // (Float) (Not Null)
    sport_interest:   userReqBody.sportInterest     // (<Array<String>>)  (Nullable)
  }

  const newUser = db.get('user').push(userObj).write()

  const userRes = {
    message: "created",
    userId: userObj.user_id,
    userName: userObj.user_name,
    userIdentifier: userObj.user_identifier
  }

  res.status(201).json(userRes)

})

server.get('/api/v1/user/:identity', (req, res) => {
  const idty = req.params.identity
  const user = db.get('user').find({ userId: idty }).value() 
  if (user === undefined) {
    const user = db.get('user').find({ userIdentifier: `@${ idty }` }).value()
    // console.log("user.if ::; ", user)
    res.status(200).json(user)
  } else {
    res.status(200).json(user)
  }  
})

server.put('/api/v1/user/:identity', (req, res) => {
  const userReqBody = req.body
  const userObj = {
    userId: userReqBody.user_id, // (String) (Not Null)
    userName: userReqBody.user_name, // (String) (Not Null)
    userIdentifier: userReqBody.user_identifier, // (String) (Not Null)
    city: userReqBody.city, // (String) (Not Null)
    country: userReqBody.country, // (String) (Not Null)
    email: userReqBody.email, // (String) (Not Null)
    phone: userReqBody.phone, // (String) (Nullable)
    birthday: userReqBody.birthday, // (String) (Nullable)
    userType: userReqBody.user_type, // (<Enumerate<String>>) (Not Null)
    isPayed: userReqBody.is_pro, // (Boolean) (Not Null)
    payedOptions: userReqBody.payed_options, // (<Enumerate<String>>) (Nullable)
    height: userReqBody.height, // (Float) (Not Null)
    weight: userReqBody.weight, // (Float) (Not Null)
    sportInterest: userReqBody.sport_interest // (<Array<String>>)  (Nullable)
  }
  const idty = req.params.identity
  const user = db.get('user').find({ userId: idty }).value() 
  if (user === undefined) {
    const user = db.get('user').find({ userIdentifier: `@${ idty }` }).assign(userObj).write()
    res.status(204).json(user)
  } else {
    const user = db.get('user').find({ userId: idty }).assign(userObj).write()
    res.status(204).json(user)
  } 
  // res.status(204).json(user)
  
})

server.delete('/api/v1/user/:identity', (req, res) => {
  const idty = req.params.identity
  const user = db.get('user').find({ userId: idty }).value() 
  if (user === undefined) {
    const user = db.get('user').remove({ userIdentifier: `@${ idty }` }).write()
    res.status(204).json({ message: 'User deleted!' })
  } else {
    const user = db.get('user').remove({ userId: idty }).write()
    res.status(204).json({ message: 'User deleted!' })
  }
})
// USERS

// TEAMS
server.post('/api/v1/team', (req, res) => {
  const teamReqBody = req.body
  const teamObj = {
    id: teamReqBody.id,
    TeamName: teamReqBody.TeamName,
    TeamPicture: teamReqBody.TeamPicture,
    TeamLocation: teamReqBody.TeamLocation,
    IsVerified: teamReqBody.IsVerified,
    Members: teamReqBody.Members
  }
  const newTeam = db.get('team').push(teamObj).write()
  res.status(201).json(teamObj)
})
// TEAMS

// MOCK TEST TDD
server.get('/api/v1/mock', (req, res) => {
  res.send({ title: 'Mocked Server' });
})
// MOCK TEST TDD

server.use(router)

const port = 3003
server.listen(port, () => {
  console.log('JSON Server is running ::; ', port)
})

// Exporting server mock app
module.exports = server
