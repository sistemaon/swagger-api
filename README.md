
### Requirements
- nodejs v10.16.0

### Run project

```zsh
npm install
```

> After installing, run _npm run dev_ in an terminal and run _npm run dev:mock_ in another terminal.

- It runs express server
```zsh
npm run dev
```

- It runs mock server 
```zsh
npm run dev:mock
```

> http://localhost:3000/api-docs - Swagger api docs endpoints operations <br>
> http://localhost:3003 - Server mock base URL <br>
> http://localhost:3003/api/v1 - Server mock URL 





<br>
<br>
<br>

---
---

### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
