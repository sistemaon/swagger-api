
# MODEL
---

### USER
```
Id: 123
First Name: Jane
Last Name: Doe
Username: j.doe ( @j.doe )
City: Lisbon
Country: Portugal
Email: jane@doe.net
Phone: (+369) - 999666333 ( (code) - phone number) )
Birth Day: 02/25/1992 ( MM/DD/YYY )
```
---

### Teams
```
Id: 321
Team Name: Las Pelotas
Team Picture: [Images]
Team Location: Portugal
Is Verified: Veryfied
Members: [USERS, NON-USERS]
```
---

### Brands
```
Id: 456
Brand Name: Adidas
Brand Logo: [Images]
```
---

### Store
```
Id: 654
Store Name: Footlocker
Store Logo: [Images]
Store Location: Portugal
```
---
